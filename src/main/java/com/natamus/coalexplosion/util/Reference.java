package com.natamus.coalexplosion.util;

public class Reference {
	public static final String MOD_ID = "coalexplosion";
	public static final String NAME = "Coal Explosion";
	public static final String VERSION = "1.3.4";
	public static final String ACCEPTED_VERSIONS = "[1.16.5]";
}
