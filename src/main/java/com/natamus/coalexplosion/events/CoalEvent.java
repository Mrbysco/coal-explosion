package com.natamus.coalexplosion.events;

import com.natamus.coalexplosion.config.ConfigHandler;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.TorchBlock;
import net.minecraft.block.WallTorchBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;

@EventBusSubscriber
public class CoalEvent
{

    private void dm(PlayerEntity p, String m)
    {
        if (ConfigHandler.GENERAL.debugMode.get())
            p.sendMessage(new StringTextComponent(m), p.getUUID());
    }


    @SubscribeEvent
    public void onBlockLeftClicked(PlayerInteractEvent.LeftClickBlock e)
    {
        World world = e.getWorld();
        if (world.isClientSide) {
            return;
        }

        ItemStack hand = e.getItemStack();
        Item thing = hand.getItem();
        BlockPos blockpos = e.getPos();
        Block block = world.getBlockState(blockpos).getBlock();

        dm(e.getPlayer(), "You left clicked " + block.getRegistryName());
        dm(e.getPlayer(), "Using: " + thing.getRegistryName());
        if (
                ConfigHandler.GENERAL.sparkerThings.get().contains(thing.getRegistryName().toString())
        ) {
//            if (
//                    ConfigHandler.GENERAL.sparkerThings.get().contains(thing.getRegistryName().toString()) ||
//                            ConfigHandler.GENERAL.sparkerTags.get().contains(thing.getTags())
//            ) {
            if (block.equals(Blocks.COAL_ORE)) {
                Vector3d blockvec = new Vector3d(blockpos.getX(), blockpos.getY(), blockpos.getZ());
                world.explode(null, new DamageSource("explosion").setExplosion(), null, blockvec.x, blockvec.y, blockvec.z, ConfigHandler.GENERAL.explosionRange.get().floatValue(), ConfigHandler.GENERAL.causeFire.get(), Explosion.Mode.DESTROY);
                e.setCanceled(true);
            }
        }

    }

    @SubscribeEvent
    public void onBlockClick(PlayerInteractEvent.RightClickBlock e)
    {
        World world = e.getWorld();
        if (world.isClientSide) {
            return;
        }

        ItemStack hand = e.getItemStack();

        Block handBlock = Block.byItem(hand.getItem());

        if (handBlock instanceof TorchBlock || handBlock instanceof WallTorchBlock) {
            BlockPos blockpos = e.getPos();
            Block block = world.getBlockState(blockpos).getBlock();
            if (block.equals(Blocks.COAL_ORE)) {
                Vector3d blockvec = new Vector3d(blockpos.getX(), blockpos.getY(), blockpos.getZ());
                world.explode(null, new DamageSource("explosion").setExplosion(), null, blockvec.x, blockvec.y, blockvec.z, ConfigHandler.GENERAL.explosionRange.get().floatValue(), ConfigHandler.GENERAL.causeFire.get(), Explosion.Mode.DESTROY);
                e.setCanceled(true);

                PlayerEntity player = e.getPlayer();
                if (!player.isCreative()) {
                    hand.shrink(1);
                }
            }
        }
    }

    @SubscribeEvent
    public void onLeftClickTorch(PlayerInteractEvent.LeftClickBlock e)
    {
        World world = e.getWorld();
        if (world.isClientSide) {
            return;
        }

        if (!ConfigHandler.GENERAL.leftClickTorch.get()) return;

        ItemStack hand = e.getItemStack();

        Block handBlock = Block.byItem(hand.getItem());

        if (handBlock instanceof TorchBlock || handBlock instanceof WallTorchBlock) {
            BlockPos blockpos = e.getPos();
            Block block = world.getBlockState(blockpos).getBlock();
            if (block.equals(Blocks.COAL_ORE)) {
                Vector3d blockvec = new Vector3d(blockpos.getX(), blockpos.getY(), blockpos.getZ());
                world.explode(null, new DamageSource("explosion").setExplosion(), null, blockvec.x, blockvec.y, blockvec.z, ConfigHandler.GENERAL.explosionRange.get().floatValue(), ConfigHandler.GENERAL.causeFire.get(), Explosion.Mode.DESTROY);
                e.setCanceled(true);

                PlayerEntity player = e.getPlayer();
                if (!player.isCreative()) {
                    hand.shrink(1);
                }
            }
        }
    }
}
